# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary : This pipeline for processing infant brain MRIs. However, it is also appliable to adull brain MRIs.
* Version : 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

1.	Pipeline for infant brain processing

Input: NIfTI files and ages in month

Output: skull striped images, N4 corrected images, tissue labeled images

Functions: 
AC-PC based alignment -> skull stripping -> N4 correction -> tissue segmentation -> longitudinal segmentation if later time-point is older than 18 months

Example: Infant_brain_processing.sh M001-01-T1.nii M001-01-T2.nii 6m, M001-02-T1.nii M001-02-T2.nii 24m -a 1 -s 1 -n 1 -seg 1

-a: default 1: AC-PC based alignment; 0: skip AC-PC based alignment
-s: default 1: skull stripping; 0: skip skull stripping
-n: default 1: N4 correction; 0: skip N4 correction
-seg: default 1: tissue segmentation; 0: skip tissue segmentation

Requirement: Caffe + Python

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact